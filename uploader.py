#!/usr/bin/env python3

import argparse
import os
import logging
import requests
from PIL import Image, UnidentifiedImageError


LOG_DATE_FMT = "%Y/%m/%d %H:%M:%S"
LOG_FORMAT = "[%(asctime)s] %(levelname).1s: %(message)s"

UPLOAD_URL = "https://service.local/images"


def parse_args():
    parser = argparse.ArgumentParser(description="Upload pictures from directory")
    parser.add_argument("path", help="Path to directory with pictures")
    args = parser.parse_args()
    return args


def upload_image(file_path):
    # Check if file is image
    try:
        Image.open(file_path)
    except UnidentifiedImageError:
        logging.warning(f"{file_path} is not image")
        return

    logging.info(f"Uploading file {file_path}")
    with open(file_path, "rb") as f:
        requests.post(UPLOAD_URL, files={"image": f})


def main():
    logging.basicConfig(format=LOG_FORMAT, datefmt=LOG_DATE_FMT, level=logging.INFO)

    args = parse_args()
    if os.path.exists(args.path) and os.path.isdir(args.path):
        for file in [
            f for f in os.listdir(args.path)
            if os.path.isfile(os.path.join(args.path, f))
        ]:
            upload_image(os.path.join(args.path, file))
    else:
        logging.fatal(f"Directory {args.path} doesn't exist")


if __name__ == "__main__":
    main()

#!/usr/bin/env python3

import json


FLOAT_PRECISION = 5


def json_precision_compare(data1, data2):
    def nested_dict_unpack(data):
        for key, value in data.items():
            if isinstance(value, dict):
                yield key, len(value)
                for k, v in nested_dict_unpack(value):
                    yield k, v
            elif isinstance(value, list):
                yield key, len(value)
                for k, v in nested_dict_unpack({i: elem for i, elem in enumerate(value)}):
                    yield k, v
            elif isinstance(value, float):
                yield key, round(value, FLOAT_PRECISION)
            else:
                yield key, value

    unpacked1 = list(nested_dict_unpack(data1))
    unpacked2 = list(nested_dict_unpack(data2))

    return unpacked1 == unpacked2


def main():
    with open("in1.json", "r") as f:
        input_dict1 = json.load(f)
    with open("in2.json", "r") as f:
        input_dict2 = json.load(f)

    print(json_precision_compare(input_dict1, input_dict2))


if __name__ == "__main__":
    main()
